import React from 'react';
import {
    View,
} from 'react-native';

import NormalCounter from './src/components/NormalCounter';
import PulseJSCounter from './src/components/PulseJSCounter';
import ReduxCounter from './src/components/ReduxCounter';
import Logger from './src/helper/Logger';


import {Provider} from 'react-redux';//Redux
import {createStore, combineReducers} from 'redux';//Redux
import counterReducer from './src/cores/store/reducers/counterReducer';//Redux

const rootReducer = combineReducers({count: counterReducer});//Redux
const store = createStore(rootReducer);//Redux


const App = () =>
{
    Logger.sendColorfulText('Rerender: Main', '#ff7fbf');

    return (
        <Provider store={store}>
            <View>
                <NormalCounter/>
                <PulseJSCounter/>
                <ReduxCounter/>
            </View>
        </Provider> /*Redux*/
    );
};


export default App;
