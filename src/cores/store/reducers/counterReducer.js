import {INCREASE_COUNT, RESET_COUNT} from '../actions/counterAction';
import Logger from '../../../helper/Logger';

const initialState = {
    something: 0,
};

export default (state = initialState, action) =>
{
    switch (action.type)
    {

        //======================================================================================================================
        //INCREASE_COUNT
        //======================================================================================================================

        case INCREASE_COUNT:

            const newSomething = state.something += 1;
            Logger.sendColorfulText(newSomething, '#fdff05');

            return {
                something: newSomething,
            };


        //======================================================================================================================
        //RESET_COUNT
        //======================================================================================================================

        case RESET_COUNT:

            Logger.sendColorfulText('0', '#fdff05');

            return {
                something: 0,
            };

        default:
            return state;
    }

}
