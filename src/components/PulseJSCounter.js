import React, {useCallback, useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

import pulseJSCore from '../cores/pulseJSCore';
import Pulse from 'pulse-framework';
import Logger from '../helper/Logger';

const PulseJSCounter = (props) =>
{
    Logger.sendColorfulText('Rerender: PulseJS', '#65a533');
    const [reset, setReset] = useState(false);


    //======================================================================================================================
    //Start Timer
    //======================================================================================================================

    const startTimer = useCallback(async (endTime) =>
    {
        setReset(false);
        while (pulseJSCore.something < endTime && !reset)
        {
            await new Promise(resolve => setTimeout(resolve, 1000));
            pulseJSCore.something += 1;
            Logger.sendColorfulText(pulseJSCore.something, '#65a533');
        }
    }, [pulseJSCore, reset]);


    //======================================================================================================================
    //End Timer
    //======================================================================================================================

    const resetTimer = useCallback(() =>
    {
        setReset(true);
        pulseJSCore.something = 0;
        Logger.sendColorfulText(pulseJSCore.something, '#65a533');
    }, []);


    //======================================================================================================================
    //Render
    //======================================================================================================================

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Counter with PulseJS</Text>
            <Text style={styles.counter}>{pulseJSCore.something}</Text>
            <View style={styles.buttonContainer}>
                <Button
                    title={'Start'}
                    onPress={async () =>
                    {
                        await startTimer(10);
                    }}
                />
                <Button
                    title={'Reset'}
                    onPress={() =>
                    {
                        resetTimer();
                    }}
                />
            </View>
        </View>
    );
};


//======================================================================================================================
//Styles
//======================================================================================================================

const styles = StyleSheet.create({
    container: {
        padding: 30,
        alignItems: 'center',

        backgroundColor: 'green',
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    counter: {
        fontSize: 15,
    },
    buttonContainer: {
        flexDirection: 'row',
    },
});

export default Pulse.React(PulseJSCounter, () => [pulseJSCore.something]);

