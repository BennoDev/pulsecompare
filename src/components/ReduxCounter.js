import React, {useCallback, useState} from 'react';
import {Button, Text, View, StyleSheet} from 'react-native';
import Logger from '../helper/Logger';
import {useDispatch, useSelector} from 'react-redux';
import * as counterActions from '../cores/store/actions/counterAction';

const ReduxCounter = (props) =>
{
    Logger.sendColorfulText('Rerender: Redux', '#fdff05');

    const dispatch = useDispatch();
    const count = useSelector(state => state.count.something);
    const [reset, setReset] = useState(false);


    //======================================================================================================================
    //Start Timer
    //======================================================================================================================

    const startTimer = async (endTime) =>
    {
        setReset(false);
        while (count < endTime && !reset)
        {
            await new Promise(resolve => setTimeout(resolve, 1000));
            dispatch(counterActions.increaseCount());
        }
    };


    //======================================================================================================================
    //End Timer
    //======================================================================================================================

    const resetTimer = useCallback(() =>
    {
        setReset(true);
        dispatch(counterActions.resetCount());
    }, []);


    //======================================================================================================================
    //Render
    //======================================================================================================================

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Counter with Redux</Text>
            <Text style={styles.counter}>{count}</Text>
            <View style={styles.buttonContainer}>
                <Button
                    title={'Start'}
                    onPress={async () =>
                    {
                        await startTimer(10);
                    }}
                />
                <Button
                    title={'Reset'}
                    onPress={() =>
                    {
                        resetTimer();
                    }}
                />
            </View>
        </View>
    );
};


//======================================================================================================================
//Styles
//======================================================================================================================

const styles = StyleSheet.create({
    container: {
        padding: 30,
        alignItems: 'center',

        backgroundColor: 'yellow',
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    counter: {
        fontSize: 15,
    },
    buttonContainer: {
        flexDirection: 'row',
    },
});

export default ReduxCounter;
