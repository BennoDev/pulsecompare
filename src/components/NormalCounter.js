import React, {useCallback, useState} from 'react';
import {Button, Text, View, StyleSheet} from 'react-native';

import normalCore from '../cores/normalCore';
import Logger from '../helper/Logger';

const NormalCounter = (props) =>
{
    Logger.sendColorfulText('Rerender: Normal', '#ff0505');
    const [reset, setReset] = useState(false);


    //======================================================================================================================
    //Start Timer
    //======================================================================================================================

    const startTimer = useCallback(async (endTime) =>
    {
        setReset(false);
        while (normalCore.something < endTime && !reset)
        {
            await new Promise(resolve => setTimeout(resolve, 1000));
            normalCore.something += 1;
            Logger.sendColorfulText(normalCore.something, '#ff0505');
        }
    }, [normalCore]);


    //======================================================================================================================
    //End Timer
    //======================================================================================================================

    const resetTimer = useCallback(() =>
    {
        setReset(true);
        normalCore.something = 0;
        Logger.sendColorfulText(normalCore.something, '#ff0505');
    }, []);


    //======================================================================================================================
    //Render
    //======================================================================================================================

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Counter</Text>
            <Text style={styles.counter}>{normalCore.something}</Text>
            <View style={styles.buttonContainer}>
                <Button
                    title={'Start'}
                    onPress={async () =>
                    {
                        await startTimer(10);
                    }}
                />
                <Button
                    title={'Reset'}
                    onPress={() =>
                    {
                        resetTimer();
                    }}
                />
            </View>
        </View>
    );
};


//======================================================================================================================
//Styles
//======================================================================================================================

const styles = StyleSheet.create({
    container: {
        padding: 30,
        alignItems: 'center',

        backgroundColor: 'red',
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    counter: {
        fontSize: 15,
    },
    buttonContainer: {
        flexDirection: 'row',
    },
});

export default NormalCounter;
